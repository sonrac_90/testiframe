
<?php
if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') { // AJAX

    exit(json_encode(array('success' => 'true')));
}


?>
<!doctype>
<html>
<head>
    <title>
        IFrame
    </title>
    <script src="js/jq.js"></script>
</head>
<body>

<script type="text/javascript" >
    $(document).ready(function(){
        var iframe = document.createElement('iframe');
        iframe.width=500;
        iframe.height=500;

        iframe.onload = function (event){

            var frameOrig = this;



            function closeFrame(redirect){
                if (redirect)
                    top.document.location.href = redirect;
            }

            $(this).contents().find('form').each(function (){ //
                $(this).submit(function (event){
                    var data = $(this).serialize();
                    event.preventDefault();
                    $.ajax({
                        url: $(this).attr('action'),
                        method: 'POST',
                        data: data,
                        dataType : 'JSON',
                        success : function (data){
                            if (data.success){
                                closeFrame('/next.php');
                            }
                        }
                    });
                    return false;
                })
            });
            $(this).contents().find('#submit').each(function (){
                $(this).click(function (event){
                    event.preventDefault();

                    // $(form:has(#submit), frameorig) - ищу тег форм во frameorig, в котором есть элемент c id submit
                    var formSelector = $('form:has(#submit)', frameOrig).serialize();
                    var form = $(formSelector).serialize();
                    var url = $(formSelector).attr('action');

                    $.ajax({
                        url: url,
                        method: 'POST',
                        data: form,
                        dataType : 'JSON',
                        success : function (data){
                            console.log(data);
                            if (data.success){
                                closeFrame('/next.php');
                            }
                        }
                    });

                    return false;
                })
            });
        }

        iframe.src = "/form.php";
        $(document.body).append(iframe);
    })
</script>
</body>
</html>